# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.contrib.auth.decorators import login_required


@login_required
def main_board(request):
    return render(request, "buh/index.html")

@login_required
def partners(request):
    return render(request, "buh/partners.html")

@login_required
def expenditures(request):
    return render(request, "buh/expenditures.html")
