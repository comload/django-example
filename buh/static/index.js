function getCookie (name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
};

$(function(){
    $.when(
        $.getJSON("/static/dx/cldr/ca-gregorian.json"),
        $.getJSON("/static/dx/cldr/numbers.json"),
        $.getJSON("/static/dx/cldr/currencies.json"),
        $.getJSON("/static/dx/cldr/supplemental/likelySubtags.json"),
        $.getJSON("/static/dx/cldr/supplemental/timeData.json"),
        $.getJSON("/static/dx/cldr/supplemental/weekData.json"),
        $.getJSON("/static/dx/cldr/supplemental/currencyData.json"),
        $.getJSON("/static/dx/cldr/supplemental/numberingSystems.json")
    ).then(function () {
        //The following code converts the got results into an array
        return [].slice.apply( arguments, [0] ).map(function( result ) {
            return result[ 0 ];
        });
    }).then(
        Globalize.load //loads data held in each array item to Globalize
    ).then(function(){
        DevExpress.config({ defaultCurrency: 'RUB' });
        DevExpress.localization.locale("ru");
        // Globalize.locale(navigator.language || navigator.browserLanguage);
        Globalize.locale("ru");
    });
});