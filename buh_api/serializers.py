from django.contrib.auth.models import User, Group
from . import models
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class MainBoardSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.MainBoard
        fields = (
            'id',
            'url',
            'op_date',
            'op_balance',
            'op_type',
            'op_expend',
            'partner',
            'note'
        )


class ExpenditureSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Expenditure
        fields = ('id', 'url', 'name')


class PartnerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Partner
        fields = ('id', 'url', 'name')
