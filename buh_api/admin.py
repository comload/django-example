# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Expenditure
from .models import MainBoard
from .models import Partner

admin.site.register(Expenditure)
admin.site.register(Partner)
admin.site.register(MainBoard)


# Register your models here.
